/**
 * Created by chenfengjuan on 16/9/27.
 */
var bomH;
var activeTab=new Array();
var $tabs=new Array(6);
$tabs[0]= $("<li role='presentation' name='smAssistant' class='active smAssistant-tab'><a href='#'>小助手管理<span class='close'>x</span></a></li>");
$tabs[1]= $("<li role='presentation' name='group' class='active group-tab'><a href='#'>群管理<span class='close'>x</span></a></li>");
$tabs[2]= $("<li role='presentation' name='message' class='active message-tab'><a href='#'>消息管理<span class='close'>x</span></a></li>");
$tabs[3]= $("<li role='presentation' name='keyword' class='active keyword-tab'><a href='#'>关键字管理<span class='close'>x</span></a></li>");
$tabs[4]= $("<li role='presentation' name='assistantGroup' class='active assistantGroup-tab'><a href='#'>小助手所在群<span class='close'>x</span></a></li>");
$tabs[5]= $("<li role='presentation' name='dataOut' class='active dataOut-tab'><a href='#'>数据导出<span class='close'>x</span></a></li>");
$().ready(function () {
    bomH=$(window).height();
    $(document.body).css("height",bomH);
    //左侧导航栏开关
    $("#parent-nav").on("click",switchNav);
    //默认tab页绑定点击事件
    $(".smAssistant-tab span").on("click",closeTab);
    //打开tab页
    $(".sidebar-child li").on("click",openTab);
    //激活tab页
    $(".nav-tabs").delegate("li","click",chooseTab);
    //群管理关键字修改绑定事件
    $("#groupTbody").delegate("a","click",modifyKey);
    $("#groupKeyList").delegate("button","click",modifyGroup);

});
//左侧导航栏开关
function switchNav() {
    if($(".sidebar-child").css("display")=="none"){
        $(".sidebar-child").css("display","block");
        $(".caret").removeClass("nav-open");
    }else{
        $(".sidebar-child").css("display","none");
        $(".caret").addClass("nav-open");
    }
}
//群管理，关键字修改
function modifyKey() {
    $(".con-box").not(".none-box").addClass("none-box");
    $("#keyModify").removeClass("none-box");
    var a=$(this).parent().parent().find("td").eq(2).text();
    $.ajax({
        type: "POST",
        url: "test/groupKey.json",
        data: {"groupCode":a},
        dataType: "json",
        success: function (data) {
            $.each(data.groupKeys,function (i, item) {
                $("#groupKeyList").append(
                "<form class='form-inline keyModify' role='form'><input disabled='true' type='text' class='form-control marginL-3' value="+item.key1+
                "><input disabled='true' type='text' class='form-control marginL-3' value="+item.key2+
                "><input disabled='true' type='text' class='form-control marginL-3' value="+item.key3+
                "><input disabled='true' type='text' class='form-control marginL-3' value="+item.key4+
                "><button type='button' class='btn btn-default'>修改</button></form>"
                );
            });
        }
    });
}
//关键字修改页面修改关键字
function modifyGroup() {
    var inputDom=$(this).parent().find("input");
    var keyContentBefore=new Array();
    keyContentBefore[0]=inputDom.eq(0).val();
    keyContentBefore[1]=inputDom.eq(1).val();
    keyContentBefore[2]=inputDom.eq(2).val();
    keyContentBefore[3]=inputDom.eq(3).val();
    if(inputDom.attr("disabled")=="disabled"){
        inputDom.attr("disabled",false);
    }else {
        inputDom.attr("disabled",true);
        //发送关键字修改后的信息
        var flag=0;//关键字信息变更标识
        var keyContentAfter=new Array();
        keyContentAfter[0]=inputDom.eq(0).val();
        keyContentAfter[1]=inputDom.eq(1).val();
        keyContentAfter[2]=inputDom.eq(2).val();
        keyContentAfter[3]=inputDom.eq(3).val();


    }

}
//激活tab页
function chooseTab() {
    var lengthArray=activeTab.length;
    activeTab[lengthArray]=$(".nav-tabs .active");
    //console.log(activeTab);
    $(".active")
        .removeClass("active");
    $(".close").addClass("none-box");
    var liName=$(this).attr("name");
    $(this)
        .addClass("active")
        .find("span").filter(".close")
        .removeClass("none-box");

    $(".con-box").not(".none-box").addClass("none-box");
    $("div[id^="+liName+"]").removeClass("none-box");

    $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    $("a[id^="+liName+"]").addClass("sidebar-click");
}
//关闭tab页
function closeTab() {
    if($(".nav-tabs li").length>1) {
        var lengthArray = activeTab.length;
        $(this).parent().parent().remove();
        $(".con-box").not(".none-box").addClass("none-box");
        var liName = activeTab[lengthArray - 1].attr("name");
        while($(".nav-tabs").has("li[name="+liName+"]").length==0){
            activeTab.splice(lengthArray - 1,1);
            lengthArray = activeTab.length;
            liName = activeTab[lengthArray - 1].attr("name");
        }
        $("div[id^=" + liName + "]").removeClass("none-box");
        activeTab[lengthArray - 1].addClass("active")
            .find(".close")
            .removeClass("none-box");
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $("a[id^=" + liName + "]").addClass("sidebar-click");
        activeTab.splice(lengthArray - 1, 1);
        //console.log(activeTab);
    }else{
        activeTab.splice(0,activeTab.length);
        $(".nav-tabs li").remove();
        $(".con-box").not(".none-box").addClass("none-box");
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
    }
}
//打开tab页
function openTab(event) {
    var lengthArray=activeTab.length;
    activeTab[lengthArray]=$(".nav-tabs .active");
    //console.log(activeTab);
    if(event.target.id=="smAssistant-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".smAssistant-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[0]);
            $(".smAssistant-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".smAssistant-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='smAssistant']").removeClass("none-box");
    }
    if(event.target.id=="group-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".group-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[1]);
            initGroupTb();//初始化群管理
            $(".group-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".group-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='group']").removeClass("none-box");
    }
    if(event.target.id=="message-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".message-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[2]);
            initMsgTb();//初始化消息管理
            $(".message-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".message-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").addClass("none-box");
        $("div[id^='message']").removeClass("none-box");
    }
    if(event.target.id=="keyword-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".keyword-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[3]);
            initKeywordTb();//初始化关键字管理
            $(".keyword-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".keyword-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='keyword']").removeClass("none-box");
    }
    if(event.target.id=="assistantGroup-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".assistantGroup-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[4]);
            $(".assistantGroup-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".assistantGroup-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='assistantGroup']").removeClass("none-box");
    }
    if(event.target.id=="dataOut-nav"){
        $(".sidebar-child").find(".sidebar-click").removeClass("sidebar-click");
        $(this).addClass("sidebar-click");
        if($(".nav-tabs").find(".dataOut-tab").length<=0){
            $(".active")
                .removeClass("active")
                .find(".close").addClass("none-box");
            $(".nav-tabs").append($tabs[5]);
            $(".dataOut-tab span").on("click",closeTab);
        }else{
            $(".active")
                .removeClass("active")
                .find(".close")
                .addClass("none-box");
            $(".dataOut-tab")
                .addClass("active")
                .find(".close")
                .removeClass("none-box");
        }
        $(".con-box").not(".none-box").addClass("none-box");
        $("div[id^='dataOut']").removeClass("none-box");
    }

}

//关键字管理页面
//关键字列表的页码
var keywordPageNum=0;
//关键字页面取得表单信息
function getKeywordForm(pages) {
    var formData=new Object();
    formData.assistant=$("#selectAssis-key").val();  //获取Select选择的Value;
    formData.key1=$("#keyword1").val();
    formData.key2=$("#keyword2").val();
    formData.edcTime=$("#edcTime").val();
    formData.page=pages;
    console.log(formData);
    return formData;
}
//提交表单
function sendKeywordForm(formData) {
    $.ajax({
        type: 'POST',
        url: "test/keyword.json",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#keywordTbody").html("");//清空keywordTbody内容
            $.each(data.keywords, function (i, item) {
                $("#keywordTbody").append(
                    "<tr><td>" + item.ID + "</td><td>" + item.key1 +
                    "</td><td>" + item.key2 + "</td><td>" + item.edc +
                    "</td><td>" + item.group + "</td><td>" + item.inGroup + "</td></tr>");
            });
            $("#totalPages").text(data.totalPages);
            $("#p1").text(keywordPageNum);
            $("#p2").text(data.totalPages);
        }
    });
}
//关键字管理，提交表单，处理返回结果
function keywordSearch() {
    keywordPageNum=1;
    var data= getKeywordForm(keywordPageNum);
    sendKeywordForm(data);

}
//初始化加载全部关键字信息的第一页
function initKeywordTb(){
    //初始化下拉菜单
    $.ajax({
        type: 'POST',
        url: "test/assistantList.json",
        data: "",
        dataType: 'json',
        success: function(data){
            $.each(data.lists, function(i, item) {
                $("#selectAssis-key").append(
                "<option>"+item.assistantName+"</option>");
            });
            $("#selectAssis-key").selectpicker('refresh');

        }
    });
    //初始化关键字列表
    keywordSearch();
}
//关键字上一页
function prevKeyPage() {
    if(keywordPageNum>1) {
        keywordPageNum--;
        var data= getKeywordForm(keywordPageNum);
        sendKeywordForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//关键字下一页
function nextKeyPage() {
    if(keywordPageNum<parseInt($("#p2").text())) {
        keywordPageNum++;
        var data= getKeywordForm(keywordPageNum);
        sendKeywordForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}

//消息管理页面
//消息列表的页码
var msgPageNum=0;
//取得消息搜索表单
function getMsgForm(pages) {
    var formData=new Object();
    formData.assistant=$("#msgAssistant").val();  //获取Select选择的Value;
    formData.message=$("#message").val();
    formData.nickname=$("#nickname").val();
    formData.msgStatus=$("#msgStatus").val();
    formData.page=msgPageNum;
    //console.log(pages);
}
//发送表单
function sendMsgForm(formData) {
    $.ajax({
        type: 'POST',
        url: "test/messageList.json",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#msgTbody").html("");//清空msgTbody内容
            $.each(data.messages, function(i, item) {
                var linkStatus="";
                if(item.groupLinkStatus.time!=undefined){
                    linkStatus+="<span>发送群链接时间：</span>"+item.groupLinkStatus.time+"<br>";
                }
                if(item.groupLinkStatus.status!=undefined){
                    linkStatus+="<span>发送群链接状态：</span>"+item.groupLinkStatus.status+"<br>";
                }
                if(item.groupLinkStatus.grpName!=undefined){
                    linkStatus+="<span>发送群名：</span>"+item.groupLinkStatus.grpName+"<br>";
                }
                if(item.groupLinkStatus.keyGroup!=undefined){
                    linkStatus+="<span>关键字匹配群：</span>"+item.groupLinkStatus.keyGroup+"<br>";
                }
                if(item.groupLinkStatus.remark!=undefined){
                    linkStatus+="<span>备注：</span>"+item.groupLinkStatus.remark;
                }
                $("#msgTbody").append(
                    "<tr><td>"+item.nickname+"</td><td>"+item.msgContent+
                    "</td><td>"+item.msgStatus+"</td><td>"+item.remind+
                    "</td><td>"+item.userNickname+"</td><td>"+item.msgSituation+
                    "</td><td>"+item.confirmStatus+"</td><td>"+item.msgTime+
                    "</td><td>"+item.responseTime+"</td><td>"+linkStatus+"</td></tr>"
                );
            });
            $("#totalPages-msg").text(data.totalPages);
            $("#p1-msg").text(msgPageNum);
            $("#p2-msg").text(data.totalPages);
            alert("搜索成功了！");
        }
    });
}
//消息管理，提交表单，处理返回结果
function msgSearch() {
    msgPageNum=1;
    var data=getMsgForm(msgPageNum);
    sendMsgForm(data);
}
//初始化加载全部消息管理信息的第一页
function initMsgTb(){
    //初始化下拉菜单
    $.ajax({
        type: 'POST',
        url: "test/assistantList.json",
        data: "",
        dataType: 'json',
        success: function(data){
            $.each(data.lists, function(i, item) {
                $("#msgAssistant").append(
                    "<option>"+item.assistantName+"</option>");
            });
            $("#msgAssistant").selectpicker('refresh');

        }
    });
    //初始化消息列表
    msgSearch();
}
//消息管理上一页
function prevMsgPage() {
    if(msgPageNum>1) {
        msgPageNum--;
        var data= getMsgForm(msgPageNum);
        sendMsgForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }

}
//消息管理下一页
function nextMsgPage() {
    if(msgPageNum<parseInt($("#p2-msg").text())) {
        msgPageNum++;
        var data= getMsgForm(msgPageNum);
        sendMsgForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}


//群管理页面
//群列表的页码
var groupPageNum=0;
//群管理页面取得表单信息
function getGroupForm(pages) {
    var formData=new Object();
    formData.assistant=$("#groupName").val();
    formData.page=pages;
    //console.log(formData);
    return formData;
}
//提交表单
function sendGroupForm(formData) {
    $.ajax({
        type: 'POST',
        url: "test/groupList.json",
        data: formData,
        dataType: 'json',
        success: function(data){
            $("#groupTbody").html("");//清空groupTbody内容
            $.each(data.groupList, function (i, item) {
                $("#groupTbody").append(
                "<tr><td>"+item.groupCode+"</td><td>"+item.groupName+
                "</td><td>"+item.total+"</td><td>"+item.area+
                "</td><td>"+item.province+"</td><td>"+item.city+
                "</td><td>"+item.department+"</td><td>"+item.subsidieryName+
                "</td><td>"+item.channel+"</td><td>"+item.alias+
                "</td><td><span>修改</span>&nbsp;&nbsp;<a>关键字管理</a></td></tr>"
                );
            });
            $("#totalPages-group").text(data.totalPages);
            $("#p1-group").text(groupPageNum);
            $("#p2-group").text(data.totalPages);
        }
    });
}
//群管理，提交表单，处理返回结果
function groupSearch() {
    groupPageNum=1;
    var data= getGroupForm(groupPageNum);
    sendGroupForm(data);

}
//查看全部
function viewAll() {
    groupPageNum=1;
    var data=new Object();
    data.assistant="";
    data.page=groupPageNum;
    sendGroupForm(data);
}
//初始化加载全部群管理信息的第一页
function initGroupTb(){
    //初始化群管理列表
    groupSearch();
}
//群信息上一页
function prevGroupPage() {
    if(groupPageNum>1) {
        groupPageNum--;
        var data= getGroupForm(groupPageNum);
        sendGroupForm(data);
        alert("上一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是第一页了哦！");
    }
}
//群信息下一页
function nextGroupPage() {
    if(groupPageNum<parseInt($("#p2-group").text())) {
        groupPageNum++;
        var data= getGroupForm(groupPageNum);
        sendGroupForm(data);
        alert("下一页：等一会哈，一会告诉你。");
    }else {
        alert("已经是最后一页了哦！");
    }
}
